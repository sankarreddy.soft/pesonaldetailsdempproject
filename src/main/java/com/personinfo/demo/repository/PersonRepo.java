package com.personinfo.demo.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.personinfo.demo.model.Person;


public interface PersonRepo extends JpaRepository<Person, Serializable> {

}
