package com.personinfo.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonalDetailsDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonalDetailsDemoApplication.class, args);
	}

}
